import torch
import torchvision


class Net(torch.nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.flatten = torch.nn.Flatten()
        self.layer1 = torch.nn.Linear(784, 800)
        torch.nn.init.normal_(self.layer1.weight.data, mean=0.0, std=0.5)
        self.layer2 = torch.nn.Linear(800, 10)
        torch.nn.init.normal_(self.layer2.weight.data, mean=0.0, std=0.5)

    def forward(self, x):
        x = self.flatten(x)
        x = torch.relu(self.layer1(x))
        x = self.layer2(x)
        return x


training_set = torchvision.datasets.MNIST('./', download=True, train=True, transform=torchvision.transforms.ToTensor())
training_loader = torch.utils.data.DataLoader(training_set)
testing_set = torchvision.datasets.MNIST("./", download=True, train=False, transform=torchvision.transforms.ToTensor())
testing_loader = torch.utils.data.DataLoader(testing_set)
net = Net()
loss_function = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(net.parameters(), lr=0.005)
scheduler = torch.optim.lr_scheduler.MultiplicativeLR(optimizer, lr_lambda=lambda epoch: 0.3)

epochs = 0
average_loss = float('inf')
while True:

    total_amount = 0
    running_loss = 0.0
    for image, label in training_loader:
        optimizer.zero_grad()
        output = net(image)
        loss = loss_function(output, label)
        loss.backward()
        optimizer.step()
        total_amount += 1
        running_loss += loss.item()
    old_average_loss = average_loss
    average_loss = running_loss / total_amount
    epochs += 1

    print(epochs, average_loss)
    torch.save(net.state_dict(), "trained_network.pth")
    if average_loss >= old_average_loss:
        break
    if epochs % 100 == 0:
        scheduler.step()

classes = [str(i) for i in range(10)]
wrong_predictions = {classname: 0 for classname in classes}
total_predictions = {classname: 0 for classname in classes}
wrongly_classified_images = []
cut_first_dimension = torch.nn.Flatten(start_dim=0, end_dim=1)
with torch.no_grad():
    for image, label in testing_loader:
        output = net(image)
        _, prediction = torch.max(output, 1)
        total_predictions[classes[label]] += 1
        if prediction != label:
            wrongly_classified_images.append(cut_first_dimension(image))
            wrong_predictions[classes[label]] += 1
torchvision.utils.save_image(tensor=wrongly_classified_images, fp="wrongly_classified_images.png", nrow=18)
total_error_rate = 100 * sum([float(wrong_predictions[i]) for i in classes]) /\
                   sum([float(total_predictions[i]) for i in classes])
print("error rate is {:.1f} %".format(total_error_rate))
for classname, wrong_count in wrong_predictions.items():
    error_rate = 100 * float(wrong_count) / total_predictions[classname]
    print("- error rate for digit {:s} is {:.1f} %".format(classname, error_rate))
